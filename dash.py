# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.dash.dash import DashAppBase


class DashApp(metaclass=PoolMeta):
    __name__ = "dash.app"

    @classmethod
    def _get_origin(cls):
        origins = super(DashApp, cls)._get_origin()
        origins.extend(
            [
                "dash.app.crm_sales",
                "dash.app.crm_service",
                "dash.app.crm_marketing",
            ]
        )
        return origins

    @classmethod
    def get_selection(cls):
        options = super(DashApp, cls).get_selection()
        options.extend(
            [
                ("crm_sales", "CRM Sales"),
                ("crm_service", "CRM Service"),
                ("crm_marketing", "CRM Marketing"),
            ]
        )
        return options


class AppCRMSales(DashAppBase):
    "App CRM Sales"
    __name__ = "dash.app.crm_sales"


class AppCRMService(DashAppBase):
    "App CRM Service"
    __name__ = "dash.app.crm_service"


class AppCRMMarketing(DashAppBase):
    "App CRM Marketing"
    __name__ = "dash.app.crm_marketing"
