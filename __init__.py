# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import ir
from . import case
from . import customer_service
from . import configuration
from . import party
from . import sale
from . import survey
from . import activity
from . import opportunity
from . import company
from . import dash
from . import commission
from . import chart


def register():
    Pool.register(
        survey.SurveyGroup,
        survey.SurveyTemplate,
        survey.SurveyTemplateLine,
        survey.Survey,
        survey.SurveyLine,
        configuration.Configuration,
        case.Case,
        customer_service.CustomerService,
        customer_service.CustomerServiceIndicatorsStart,
        customer_service.EfficacyMonthStart,
        party.Party,
        sale.Sale,
        activity.Activity,
        opportunity.Prospect,
        opportunity.Opportunity,
        opportunity.LeadOrigin,
        opportunity.CrmOpportunityLine,
        # opportunity.Person,
        opportunity.OpportunityCancellReason,
        opportunity.OpportunitySaleConditions,
        ir.Cron,
        company.Company,
        dash.DashApp,
        dash.AppCRMSales,
        dash.AppCRMService,
        dash.AppCRMMarketing,
        commission.Agent,
        chart.CRMChart,
        module='crm', type_='model')
    Pool.register(
        customer_service.CustomerServiceReport,
        customer_service.CustomerServiceIndicators,
        customer_service.EfficacyMonth,
        customer_service.PrintCustomerService,
        module='crm', type_='wizard')
    Pool.register(
        customer_service.CustomerServiceReport,
        customer_service.CustomerServiceIndicatorsReport,
        customer_service.EfficacyMonthReport,
        customer_service.MonitoringReport,
        survey.SurveyReport,
        opportunity.OpportunityReport,
        opportunity.OpportunityOnlyReport,
        opportunity.OpportunityWithoutTaxReport,
        opportunity.OpportunityLargeReport,
        module='crm', type_='report')
